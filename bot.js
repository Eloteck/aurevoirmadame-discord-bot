const Discord = require('discord.js');
const http = require('http');
const config = require('./config.js');
const bot = new Discord.Client();

const DISCORD_TOKEN = config.discord_token;
const TUMBLR_TOKEN = config.tumblr_token;

var guild;
var channel;

bot.on('ready', function () {
    var guilds = bot.guilds;
    console.log("Connected to :");
    guilds.forEach(function (g) {
        console.log(g.name);
        guild = g;
    });
    channel = guild.channels.first();
});

bot.on('message', function (message) {
    if (message.content === '!madame') {
        var help = ".\n ==== MADAME HELP ==== \n" +
            "!madame : show this message \n" +
            "!madame-today : show today madame\n" +
            "!madame-random : show random madame between last 50 posts\n"+
            "!madame-settings channel {id} : send messages in specific channel. Get channel id : https://github.com/Chikachi/DiscordIntegration/wiki/How-to-get-a-token-and-channel-ID-for-Discord#get-the-channel-id-of-the-discord-text-channel\n" +
            "!madame-settings channel : get channel wich messages from Madame are send.\n";
        channel.send(help);
    }
});

bot.on('message', function (message) {
    var mes = message.content.split(' ');
    if (mes[0] === '!madame-settings') {
        if (mes[1] === "channel") {
            if (mes[2] === undefined) {
                message.channel.send('Messages from Madame are send to #' + channel.name);
                return;
            }
            channel = guild.channels.get(mes[2]);
            message.channel.send("Messages from Madame will be send to #" + channel.name);
        }
    }
});

bot.on('message', function (message) {
    if (message.content === '!madame-today') {
        getAurevoirMadameLastPost(function (image) {
            channel.send(image);
        });
    }
});

bot.on('message', function (message) {
    if (message.content === '!madame-random') {
        getRandomMadame(function (image) {
            channel.send(image);
        });
    }
});

function getAurevoirMadameLastPost(callback) {
    getMadame('posts', 'limit=1', function (parsed) {
        callback(parsed.posts[0].photos[0].original_size.url);
    });
}

function getRandomMadame(callback) {
    getMadame('posts', '&limit=50', function (parsed) { // 50 is the max from Tumblr
        var rand = Math.floor((Math.random() * parsed.posts.length ) + 1);
        callback(parsed.posts[rand].photos[0].original_size.url);

    })
}

function getMadame(info, parameters, callback) {
    return http.get({
        host: "api.tumblr.com",
        path: '/v2/blog/aurevoirmadame.fr/'+ info +'?api_key=' + TUMBLR_TOKEN + '&' + parameters
    }, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            // Data reception is done, do whatever with it!
            var parsed = JSON.parse(body);
            callback(parsed.response);
        });
    });
}


bot.login(DISCORD_TOKEN);